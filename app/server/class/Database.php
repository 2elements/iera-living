<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 24/10/15
 * Time: 2:41 PM
 */

class Database {

    const HOST = 'localhost';
    const USERNAME = 'root';
    const PASSWORD = 'root';
    const DATABASE = 'iera_living';


    const SUCCESS = true;
    const FAILURE = false;

    private $conn;  //connection object

    private $response;  //connection response

    /**
     * Database constructor
     */
    public function __construct(){

    }

    public function createConnection(){

        $dsn = 'mysql:host=' . self::HOST . ';dbname=' . self::DATABASE;
        $username = self::USERNAME;
        $password = self::PASSWORD;

        try{
            $this->conn = new PDO($dsn, $username, $password);
            $this->response['conn_res'] = self::SUCCESS;
        }catch (PDOException $e){
            $this->response['conn_res'] = self::FAILURE;
        }
    }

    public function insertQuery($query, $params = array()){

        $this->createConnection();

        $sq = $this->conn->prepare($query);
        if($sq->execute($params)){
            $response['sql_status'] = self::SUCCESS;
        }else{
            $response['sql_status'] = self::FAILURE;
        }
        $response['insert_id'] = $this->conn->lastInsertId();

        $this->closeConnection();

        $this->response['sql_res'] = $response['sql_status'];
        $this->response['insert_id'] = $response['insert_id'];
    }


    public function selectQuery($query, $params = array()){

        $this->createConnection();

        $sq = $this->conn->prepare($query);
        $sq->execute($params);

        $results_array = $sq->fetchAll(PDO::FETCH_ASSOC);
        $rows_num = count($results_array);

        if($rows_num > 0){
            $response['sql_status'] = self::SUCCESS;
            $response['sql_data'] = $results_array;
        }else{
            $response['sql_status'] = self::FAILURE;
            $response['sql_data'] = null;
        }

        $this->closeConnection();

        $this->response['sql_res'] = $response['sql_status'];
        $this->response['sql_data'] = $response['sql_data'];
    }

    public function deleteQuery($query, $params = array()){

        $this->createConnection();

        $sq = $this->conn->prepare($query);
        if($sq->execute($params)){
            $response['sql_status'] = self::SUCCESS;
        }else{
            $response['sql_status'] = self::FAILURE;
        }

        $this->closeConnection();

        $this->response['sql_res'] = $response['sql_status'];
    }

    public function closeConnection(){
        /*$this->conn->close();*/
    }

    public function getResponse(){
        return $this->response;
    }

}