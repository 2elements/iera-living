CREATE TABLE users
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    email VARCHAR(60) NOT NULL,
    password VARCHAR(100) NOT NULL,
    date_created DATETIME
);

CREATE TABLE categories
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    type INT NOT NULL,
    position INT NOT NULL,
    page INT NOT NULL
);

CREATE TABLE feedbacks
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(60) NOT NULL,
    message TEXT NOT NULL,
    company VARCHAR(100),
    phone_number VARCHAR(12)
);