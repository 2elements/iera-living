<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 7/11/15
 * Time: 7:39 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();

$response = array();
$response['errors'] = array();

if(!isset($_POST['name']) || empty($_POST['name'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Name is required';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

if(!isset($_POST['email']) || empty($_POST['email'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Email is required';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}

if(!isset($_POST['message']) || empty($_POST['message'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Message is required';
    $response['errors']['code'] = 3;

    echo json_encode($response);
    exit(0);
}

$name = $_POST['name'];
$email = $_POST['email'];
$company = $_POST['company'];
$number = $_POST['number'];
$message = $_POST['message'];

$query = 'INSERT INTO feedbacks(name, email, company, phone_number, message) VALUES(:name, :email, :company, :number, :message)';
$database->insertQuery($query, array(':name' => $name, ':email' => $email, ':company' => $company, ':number' => $number, ':message' => $message));
$res = $database->getResponse();


if($res['sql_res'] == false){
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in inserting';
    $response['errors']['code'] = 4;
}else{
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);