<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 3/11/15
 * Time: 1:56 AM
 */

ob_start();
session_start();

$response = array();
$response['code'] = 0;

if (!isset($_SESSION['user']['id'])) {
    $response['message'] = "Not logged In";
    $response['code'] = 0;
    exit(0);
}

if (isset($_FILES['file'])) {
    $errors = array();
    $file_name = $_FILES['file']['name'];
    $file_size = $_FILES['file']['size'];
    $file_tmp = $_FILES['file']['tmp_name'];
    $file_type = $_FILES['file']['type'];
    $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
    $extensions = array("jpeg", "jpg", "png");
    if (in_array($file_ext, $extensions) == false) {
        $errors[] = "image extension not allowed, please choose a JPEG or PNG file.";
    }
    if ($file_size > 2097152) {
        $errors[] = 'File size cannot exceed 2 MB';
    }
    if (empty($errors) == true) {
        $user_id = $_SESSION['user']['id'];
        $file_name = $_POST['category_id'] . "_" . $_POST['index_id'] . ".jpg";
        move_uploaded_file($file_tmp, "images/category/" . $file_name);

        $response['file'] = $file_name;
        $response['message'] = "Upload Success";
        $response['code'] = 1;
        echo json_encode($response);
    } else {
        $response['errors'] = $errors;
        echo json_encode($response);
    }
} else {
    $errors = array();
    $errors[] = "No image found";

    $response['errors'] = $errors;
    echo json_encode($response);
}

?>