<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 28/10/15
 * Time: 6:06 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();

$response = array();
$response['errors'] = array();

if(!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged In.';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

$id = $_POST['id'];

$query = 'DELETE FROM categories WHERE id = :id';
$database->insertQuery($query, array(':id' => $id));
$res = $database->getResponse();

if ($res['sql_res'] == false) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in deleting';
    $response['errors']['code'] = 2;
} else {
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
    $response['data'] = $res['sql_data'];
}

echo json_encode($response);
exit(0);
