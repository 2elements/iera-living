<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 24/10/15
 * Time: 4:18 PM
 */

ob_start();
session_start();

//forcing the browser to not cache!
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");



if(isset($_SESSION['user']['id']) && !empty($_SESSION['user']['id'])){
    $data['id'] = $_SESSION['user']['id'];
    $data['email'] = $_SESSION['user']['email'];
    $data['status'] = 1;
}else{
    $data['status'] = 0;
}

echo json_encode($data);

?>