<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 23/10/15
 * Time: 4:45 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();


$response = array();
$response['errors'] = array();

if(!isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['password']) || empty($_POST['password'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input.';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

$email = $_POST['email'];
$password = $_POST['password'];


$query = 'SELECT id from users WHERE email = :email AND password = :password LIMIT 1';

$database->selectQuery($query, array(':email' => $email, ':password' => $password));
$res = $database->getResponse();


if($res['sql_res'] == true && $res['sql_data'] != null ){

    $_SESSION['user'] = array();
    $_SESSION['user']['id'] = $res['sql_data'][0]['id'];
    $_SESSION['user']['email'] = $email;

    $response['code'] = 1;
    $response['data'] = $_SESSION['user'];
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}else{
    $response['code'] = 0;
    $response['errors']['message'] = 'Invalid Email Address or Password.';
    $response['errors']['code'] = 2;
}

echo json_encode($response);