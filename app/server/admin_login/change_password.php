<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 25/10/15
 * Time: 2:39 AM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();


$response = array();
$response['errors'] = array();

if (!isset($_POST['newpwd']) || !isset($_POST['confirmpwd'])) {
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input.';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}


if(!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged In.';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}


//$oldpwd = $_POST['oldpwd'];
$newpwd = $_POST['newpwd'];
$confirmpwd = $_POST['confirmpwd'];


if ($newpwd !== $confirmpwd) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Password does not match';
    $response['errors']['code'] = 3;

    echo json_encode($response);
    exit(0);
}

if (strlen($newpwd) < 6) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Password length is less than 6.';
    $response['errors']['code'] = 4;

    echo json_encode($response);
    exit(0);
}

$id = $_SESSION['user']['id'];
$query = 'UPDATE users SET password = :newpwd WHERE id = :id';
$database->insertQuery($query, array(':newpwd' => $newpwd, ':id' => $id));
$res = $database->getResponse();

if ($res['sql_res'] == false) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in updating';
    $response['errors']['code'] = 5;
    $response['tmp'] = $res;
} else {
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
    $response['tmp'] = $res;
}

echo json_encode($response);
exit(0);
