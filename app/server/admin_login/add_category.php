<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 27/10/15
 * Time: 4:45 AM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();


$response = array();
$response['errors'] = array();


if(!isset($_POST['name']) || !isset($_POST['type']) || !isset($_POST['position']) || !isset($_POST['page']) || empty($_POST['name']) || empty($_POST['type']) || empty($_POST['position']) || empty($_POST['page'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}


if(!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged in';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}


$name = $_POST['name'];
$type = $_POST['type'];
$position = $_POST['position'];
$page = $_POST['page'];

$query = 'INSERT INTO categories(name, type, position, page) VALUES(:name, :type, :position, :page)';
$database->insertQuery($query, array(':name' => $name, ':type' => $type, ':position' => $position, ':page' => $page));
$res = $database->getResponse();


if($res['sql_res'] == false){
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in inserting';
    $response['errors']['code'] = 3;
}else{
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);