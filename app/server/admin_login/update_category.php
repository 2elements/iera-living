<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 28/10/15
 * Time: 5:34 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();

$response = array();
$response['errors'] = array();

if (!isset($_POST['name']) || !isset($_POST['position']) || empty($_POST['name']) || empty($_POST['position'])) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Fields should contain value.'.$_POST['position'];
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}


if(!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged In.';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}

$id = $_POST['id'];
$name = $_POST['name'];
$position = $_POST['position'];

$query = 'UPDATE categories SET name = :name, position = :position WHERE id = :id';
$database->insertQuery($query, array(':name' => $name, ':position' => $position, ':id' => $id));
$res = $database->getResponse();

if ($res['sql_res'] == false) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in updating';
    $response['errors']['code'] = 3;
} else {
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);
exit(0);
