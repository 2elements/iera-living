'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', ['ngRoute', 'ngAnimate', 'ngFileUpload', 'dcbImgFallback', 'angular-loading-bar', 'angular.filter']);


app.run(['$rootScope', function($rootScope) {

    $rootScope.setLogin = function(data){

        $rootScope.user = {
            id: data.id,
            email: data.email,
            status: data.status
        };

    };

    $rootScope.unsetLogin = function(){
        $rootScope.user = {
            id: '',
            email: '',
            password: '',
            status: 0
        };
    };

    $rootScope.categoryPicture = {
        path: '/server/admin_login/images/category/',
        ext: '.jpg',
        fallbackImg: '/server/admin_login/images/category/misc/no-image.jpg'
    };

}]);




app.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
            input.push(i);
        }

        return input;
    };
});