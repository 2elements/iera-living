/**
 * Created by aditya on 7/11/15.
 */

app.controller('productController', ['$scope', '$http', '$location', '$routeParams', function ($scope, $http, $location, $routeParams) {

    $scope.page = 0;

    switch ($location.$$path){
        case '/sourcing':
            $scope.page = 1;
            break;
        case '/design':
            $scope.page = 2;
            break;
        case '/product':
            $scope.page = 3;
            break;
    }

    $scope.myCategory = [];

    console.info("Path is", $scope.page);

    $scope.getData = function () {
        $http({
            url: 'server/admin_login/category_data.php',
            method: 'GET',
            params: {
                page: $scope.page
            },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Category response", data);

                $scope.myCategory = [];

                if (data.code == 1) {
                    $scope.myCategory = data.data;
                }

                $scope.arrayLength = $scope.myCategory.length;
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
            });
    };

    $scope.getData();

}]);
