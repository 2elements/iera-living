/**
 * Created by deepak on 23/10/15.
 */

app.controller('loginController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    $scope.form = {
        text: 'Login',
        active: false
    };

    $scope.user = {
        email: '',
        password: ''
    };

    $scope.login = function () {
        $scope.loginMsg = ''; //clearing the error message on new data submission
        $scope.toggleLoginButton(0);

        console.log("Submitting data", $scope.user);


        $http({
            url: 'server/admin_login/login.php',
            method: 'POST',
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Login Response", data);

                if (data.code == 1) {
                    //login complete
                    $rootScope.setLogin(data);

                    console.info('going to category page');
                     $location.path('/category');

                    $scope.user = {
                        email: '',
                        password: ''
                    };
                }
                else {

                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.loginMsg = 'Provide inputs.';
                        }
                            break;
                        case 2:
                        {
                            $scope.loginMsg = 'Invalid Email Address or Password.';
                            //$scope.user.email = ''; //let the email remain
                            $scope.user.password = '';
                        }
                            break;
                        case 3:
                        { //there is no code 3 in php //this is never used
                            $scope.loginMsg = 'Check your email to complete registration';
                        }
                            break;
                        default :
                        {
                            $scope.loginMsg = 'Sorry, An unknown error occurred.';
                        }
                    }
                }

                $scope.toggleLoginButton(1);

            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.loginMsg = 'Sorry, An unknown error occurred.';
            });
    };


    $scope.toggleLoginButton = function (type) {
        $scope.form = {
            text: type == 1 ? 'Login' : 'Working...',
            active: type != 1
        };

    };

}]);