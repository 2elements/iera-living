/**
 * Created by deepak on 27/10/15.
 */


app.controller('categoryController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {

    $scope.category = {
        name: '',
        type: '',
        position: '',
        page: ''
    };

    $scope.add = {
        text: 'Add',
        active: false
    };

    $scope.myCategory = [];
    $scope.arrayLength = 0;

    $scope.errorMsg = [];

    $scope.saveButtons = [];
    $scope.removeButtons = [];

    $scope.$watch(function () {
        return $scope.myCategory.length
    }, function (newVal, oldVal) {
        console.log("watch", $scope.myCategory.length);
        $scope.arrayLength = $scope.myCategory.length;
    });


    /*TODO Category Data*/

    $scope.getData = function () {
        $http({
            url: 'server/admin_login/category_data.php',
            method: 'GET',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Category response", data);

                $scope.myCategory = [];

                if (data.code == 1) {
                    $scope.myCategory = data.data;
                }
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
            });
    };

    $scope.getData();


    /*TODO category actions*/

    $scope.addCategory = function () {
        console.log("Submitting data", $scope.category);
        $scope.toggleAddButton(0);

        $http({
            url: 'server/admin_login/add_category.php',
            method: 'POST',
            data: $.param($scope.category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Add category response", data);

                if (data.code == 1) {

                    console.log("Category added successfully", data);

                    $scope.myCategory.push($scope.category);

                    $scope.category = {
                        name: '',
                        type: '',
                        order: ''
                    };

                } else {

                    switch (data.errors.code) {

                        case 1:
                        {
                            $scope.addCategoryMsg = 'Please fill the complete form.';
                        }
                            break;
                        case 2:
                        {
                            $scope.addCategoryMsg = 'Please login again to and retry.';
                            $scope.category = {
                                name: '',
                                type: '',
                                position: ''
                            };
                        }
                            break;
                        default:
                        {
                            $scope.addCategoryMsg = 'Sorry, An unknown error occurred. Please retry!';
                        }
                            break;

                    }

                }

                $scope.toggleAddButton(1);
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.addCategoryMsg = 'Sorry, An unknown error occurred. Please retry!';
            });

    };


    $scope.saveItem = function (category, index) {
        console.log("Data received", category);
        $scope.toggleSaveButton(0, index);

        $scope.errorMsg = [];

        $http({
            url: 'server/admin_login/update_category.php',
            method: 'POST',
            data: $.param(category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Update category response", data);

                if (data.code == 1) {

                    console.log("Category updated successfully", data);

                } else {

                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.errorMsg[index] = "Fields should contain value.";
                        }
                            break;
                        case 2:
                        {
                            $scope.errorMsg[index] = "Please login again to and retry.";
                        }
                            break;
                        case 3:
                        {
                            $scope.errorMsg[index] = "Sorry, An unknown error occurred. Please retry!";
                        }
                            break;
                        default :
                        {
                            $scope.errorMsg[index] = "Sorry, An unknown error occurred. Please retry!";
                        }
                            break;
                    }

                }

                $scope.toggleSaveButton(1, index);
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                console.log('Sorry, An unknown error occurred. Please retry!');
            });

    };


    $scope.removeItem = function (id, index) {
        console.log("Data received", id);
        $scope.toggleRemoveButton(0, index);

        $scope.errorMsg = [];

        $http({
            url: 'server/admin_login/delete_category.php',
            method: 'POST',
            data: $.param({
                id: id
            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Delete category response", data);

                if (data.code == 1) {
                    console.log("Category deleted successfully", data);
                } else {

                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.errorMsg[index] = "Please login again to and retry.";
                        }
                            break;
                        case 2:
                        {
                            $scope.errorMsg[index] = "Sorry, An unknown error occurred. Please retry!";
                        }
                            break;
                        default :
                        {
                            $scope.errorMsg[index] = "Sorry, An unknown error occurred. Please retry!";
                        }
                            break;
                    }

                }

                $scope.getData();

                $scope.toggleRemoveButton(1, index);

            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                //console.log('Sorry, An unknown error occurred. Please retry!');
            });

    };


    $scope.toggleSaveButton = function (type, index) {
        $scope.saveButtons[index] = type;
    };

    $scope.toggleRemoveButton = function (type, index) {
        $scope.removeButtons[index] = type;
    };

    $scope.toggleAddButton = function (type) {
        $scope.add = {
            text: type == 1 ? 'Add' : 'Adding...',
            active: type != 1
        };

    };


}]);