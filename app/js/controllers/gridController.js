/**
 * Created by aditya on 7/11/15.
 */


app.controller('gridController', ['$scope', '$rootScope', '$http', 'Upload', function ($scope, $rootScope, $http, Upload) {


    $scope.categoryImage = [];
    $scope.imageLoading = [];

    for (var i = 1; i <= 5; i++){
        $scope.categoryImage[i] = $rootScope.categoryPicture.path + $scope.data.id + '_' + i + $rootScope.categoryPicture.ext;
        $scope.imageLoading[i] = false;
    }

    console.log("Starting", $scope.imageLoading);


        $scope.clickImageInput = function (categoryId, indexId) {
            /*if($rootScope.user.status !== 1){
             console.log("Please login to add images");
             return;
             }*/

            //console.log("Ids", categoryId, indexId);

            $(':input[id="' + categoryId + indexId + '"]').click();
        };

    $scope.submitCategoryImage = function (image, categoryId, indexId) {
        if (typeof  image !== 'undefined' && image != null) {
            console.log(image);
            $scope.uploadBackground([image], categoryId, indexId);
        }
    };

    $scope.uploadBackground = function ($files, categoryId, indexId) {
        console.log("Upload", $files[0].type.indexOf('image'));

        $scope.imageLoading[indexId] = true;

        /*if($rootScope.user.status !== 1){
         console.log("Please login to add images");
         return;
         }*/

        var url = 'server/admin_login/upload_category_image.php';


        if ($files.length === 0) {
            return;
        }

        var file = $files[0];

        if (file.type.indexOf('image') == -1) {
            $scope.error = 'image extension not allowed, please choose a JPEG or PNG file.';
            return;
        }

        if (file.size > 2097152) {
            $scope.error = 'File size cannot exceed 2 MB';
            return;
        }

        $scope.uploadFile = Upload.upload({
            url: url,
            data: {file: file, category_id: categoryId, index_id: indexId}
        }).success(function (data, status, headers, config) {
            console.log("SUCCESS", data.code);

            if (data.code == 1) {
                //$scope.categoryPicture.path = data.file;
                console.log("Image upload", data.message);
                console.log("Profile picture uploaded");
                console.log($scope.categoryImage[indexId]);
                $scope.categoryImage[indexId] = $scope.categoryImage[indexId] + '?' + new Date().getTime();
            } else {
                console.error("Image Upload", data.message);
            }

            $scope.imageLoading[indexId] = false;
        })
            .error(function (data, status, headers, config) {
                console.log("Error Occurred while uploading image");
                $scope.imageLoading[indexId] = false;
            });
    };


}]);