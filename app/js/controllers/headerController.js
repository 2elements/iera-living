/**
 * Created by aditya on 12/10/15.
 */


app.controller('headerController', ['$scope', function($scope){

    $scope.menuState = false;

    $scope.menuToggle = function(){
        $scope.menuState = !$scope.menuState;
    };
}]);