/**
 * Created by deepak on 7/11/15.
 */

app.controller('feedbackController', ['$scope', '$rootScope', '$http', '$timeout', function ($scope, $rootScope, $http, $timeout) {

    $scope.user = {
        name: '',
        company: '',
        email: '',
        number: '',
        message: ''
    };

    $scope.successMsg = false;

    $scope.contactUs = function() {
        console.log("Submitting data", $scope.user);
        $scope.errorMsg = '';

        $http({
            url: 'server/feedback.php',
            method: 'POST',
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Feedback Response", data);

                if (data.code == 1) {

                    console.log('Response recorded');

                    $scope.successMsg = true;

                    $scope.user = {
                        name: '',
                        company: '',
                        email: '',
                        number: '',
                        message: ''
                    };
                }
                else {

                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.errorMsg = '* Name is required.';
                        }
                            break;
                        case 2:
                        {
                            $scope.errorMsg = 'Email is required.';
                        }
                            break;
                        case 3:
                        {
                            $scope.errorMsg = 'Message is required.';
                        }
                            break;
                        case 4:
                        {
                            $scope.errorMsg = 'Sorry, An unknown error occurred.';
                        }
                            break;
                        default :
                        {
                            $scope.errorMsg = 'Sorry, An unknown error occurred.';
                        }
                    }
                }
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.errorMsg = 'Sorry, An unknown error occurred.';
            });

    };


    $scope.closeDialog = function () {
        $scope.successMsg = false;
    }

}]);
