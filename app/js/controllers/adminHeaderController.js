/**
 * Created by aditya on 8/11/15.
 */

app.controller('adminHeaderController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    /*TODO-me causing undefined error*/
    /*if(typeof $rootScope.user === 'undefined' || typeof $rootScope.user.status === 'undefined' || $rootScope.user.status != 1){
        console.log("Not Logged In", $rootScope.user);
        $rootScope.unsetLogin();
        $location.path('/login');
    }*/

    $http.get('server/admin_login/check_login.php')
        .success(function (data) {

            console.log("[#SERVER]Check Login", data);
            if(data.status == 1){
                $rootScope.setLogin(data);
            }else{
                $rootScope.unsetLogin();
                $location.path('/login');
            }
        })
        .error(function(data, status, headers, config){
            console.log("[#Header-checkLogin] Login error", data,"status", status,"headers", headers,"config", config);
        });


    $scope.logout = function () {
        $http({
            url: 'server/admin_login/logout.php'
        })
            .success(function (data) {
                console.log("Logged out user!");
                $rootScope.unsetLogin();
                $location.path('/login');
            })
            .error(function (data, status, headers, config) {
                console.log("Logout error", data, "status", status, "headers", headers, "config", config);
            });

    };

}]);