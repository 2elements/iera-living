/**
 * Created by deepak on 25/10/15.
 */


app.controller('changePasswordController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {


    $scope.form = {
        text: 'Change Password',
        active: false
    };

    $scope.user = {
        newpwd: '',
        confirmpwd: ''
    };

    $scope.successMsg = '';
    $scope.changepwdMsg = '';

    $scope.changePassword = function () {
        console.log("Submitting data", $scope.user);

        $scope.changepwdMsg = '';
        $scope.successMsg = '';

        if ($scope.user.newpwd !== $scope.user.confirmpwd) {
            $scope.changepwdMsg = 'New password and confirm password does not match.';
            return;
        }
        if ($scope.user.newpwd.length < 6) {
            $scope.changepwdMsg = 'Passwords must be at least 6 characters long.';
            return;
        }

        $scope.toggleLoginButton(0);

        $http({
            url: 'server/admin_login/change_password.php',
            method: 'POST',
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Change password response", data);

                if (data.code == 1) {

                    console.log("Password changed successfully");
                    $scope.successMsg = 'Password changed successfully.';

                    $scope.user = {
                        newpwd: '',
                        confirmpwd: ''
                    };



                } else {
                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.changepwdMsg = 'Please fill the complete password form.';
                        }break;
                        case 2:
                        {
                            $scope.changepwdMsg = 'Please login again to and retry.';
                            $scope.user = {
                                newpwd: '',
                                confirmpwd: ''
                            };
                        }break;
                        case 3:
                        {
                            $scope.changepwdMsg = 'New password and confirm password does not match.';
                            $scope.user = {
                                newpwd: '',
                                confirmpwd: ''
                            };
                        }
                            break;
                        case 4:
                        {
                            $scope.changepwdMsg = 'New password length is less than 6.';
                        }
                            break;
                        default :
                        {
                            $scope.changepwdMsg = 'Sorry, An unknown error occurred. Please retry!';
                        }
                    }
                }

                $scope.toggleLoginButton(1);

            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.changepwdMsg = 'Sorry, An unknown error occurred. Please retry!';
                $scope.toggleLoginButton(1);
            });
    };

    $scope.toggleLoginButton = function (type) {
        $scope.form = {
            text: type == 1 ? 'Change Password' : 'Working...',
            active: type != 1
        };

    };


}]);

