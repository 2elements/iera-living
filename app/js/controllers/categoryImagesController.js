/**
 * Created by deepak on 3/11/15.
 */

app.controller('categoryImagesController', ['$scope', '$rootScope', '$http', 'Upload', function ($scope, $rootScope, $http, Upload) {


    $scope.myCategory = [];

    $scope.getData = function () {
        $http({
            url: 'server/admin_login/category_data.php',
            method: 'GET',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Category response", data);

                $scope.myCategory = [];

                if (data.code == 1) {
                    $scope.myCategory = data.data;
                }

                $scope.arrayLength = $scope.myCategory.length;
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
            });
    };

    $scope.getData();

}]);