/**
 * Created by aditya on 10/10/15.
 */


app.directive('headerCustom', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/header.html',
        controller: 'headerController'
    };
});

app.directive('gridA', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/grid_a.html',
        controller: 'gridController',
        scope: {
            data: '=',
            type: '='
        }
    };
});

app.directive('gridB', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/grid_b.html',
        controller: 'gridController',
        scope: {
            data: '=',
            type: '='
        }
    };
});

app.directive('imageLoading', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/image_loading.html'
    };
});

app.directive('adminHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/admin_header.html',
        controller: 'adminHeaderController'
    };
});



/*
app.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function() {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
                if(iAttrs.fallbackSrc == "")
                    angular.element(this).attr("class", "");
            });
        }
    };
    return fallbackSrc;
});
*/
