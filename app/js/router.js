/**
 * Created by aditya on 10/10/15.
 */

app.config(['$routeProvider', function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/home_view.html'
        })
        .when('/sourcing', {
            templateUrl: 'views/sourcing.html',
            controller: 'productController'
        })
        .when('/design', {
            templateUrl: 'views/design.html',
            controller: 'productController'
        })
        .when('/product', {
            templateUrl: 'views/product.html',
            controller: 'productController'
        })
        .when('/contact', {
            templateUrl: 'views/contact.html',
            controller: 'feedbackController'
        })
        .when('/login', {
            templateUrl: 'views/admin/login.html',
            controller: 'loginController'
        })
        .when('/change-password', {
            templateUrl: 'views/admin/change_password.html',
            controller: 'changePasswordController'
        })
        .when('/category', {
            templateUrl: 'views/admin/category.html',
            controller: 'categoryController'
        })
        .when('/category-images', {
            templateUrl: 'views/admin/category-images.html',
            controller: 'categoryImagesController'
        })
        .otherwise({redirectTo: '/'});
}]);
