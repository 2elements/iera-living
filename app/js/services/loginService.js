/**
 * Created by deepak on 24/10/15.
 */

app.service('loginService', ['$http', '$rootScope', '$log', function($http, $rootScope, $log) {

    return $http.get('server/admin_login/login.php')
        .success(function(data){
            console.log("Login Service data", data);

            if(data.status == 1){
                $rootScope.setLogin(data);
            }else{
                $rootScope.unsetLogin(data);
            }

            return data;
        })
        .error(function(data, status, headers, config){
            console.log("error", data,"status", status,"headers", headers,"config", config);
        });
}]);