module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        uglify: {
            options: {
                sourceMap: true,
                mangle: false,
                compress: {
                    drop_console: true
                },
                preserveComments: false
            },
            build: {
                files: {
                    /*'app/js/build/angular_router.min.js': [
                     'app/bower_components/angular/angular.min.js',
                     'app/bower_components/angular-route/angular-route.min.js'
                     ],*/
                    'app/js/build/angular.min.js': [
                        'app/bower_components/angular/angular.min.js'
                    ],
                    'app/js/build/angular-route.min.js': [
                        'app/bower_components/angular-route/angular-route.min.js'
                    ],
                    'app/js/build/angular_modules.min.js': [
                        'app/bower_components/angular-animate/angular-animate.min.js',
                        'app/bower_components/angular-loading-bar/build/loading-bar.min.js',
                        'app/bower_components/ng-file-upload/ng-file-upload.min.js',
                        'app/bower_components/angular-img-fallback/angular.dcb-img-fallback.min.js',
                        'app/bower_components/angular-filter/dist/angular-filter.min.js',
                        'app/bower_components/jquery/dist/jquery.min.js',
                        'app/bower_components/bootstrap/dist/js/bootstrap.min.js'
                    ],
                    'app/js/build/app.js': [
                        'app/js/app.js'
                    ],
                    'app/js/build/all.min.js': [
                        'app/js/router.js',
                        'app/js/directives/*.js',
                        'app/js/controllers/*.js'
                    ]
                }
            }
        },
        cssmin: {
            combine: {
                options: {
                    sourceMap: true,
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: {
                    'app/styles/build/all.css': [
                        'app/bower_components/angular-loading-bar/build/loading-bar.min.css',
                        'app/bower_components/bootstrap/dist/css/bootstrap.min.css',
                        'app/styles/css/app.css',
                        'app/styles/css/animations.css',
                        'app/styles/css/image_loading.css',
                        'app/styles/css/home_view.css',
                        'app/styles/css/header.css',
                        'app/styles/css/admin_login.css',
                        'app/styles/css/category.css',
                        'app/styles/css/admin_header.css'
                    ]
                }
            },
            minify: {
                expand: true,
                cwd: 'app/styles/build/',
                src: ['*.css', '!*.min.css'],
                dest: 'app/styles/build/',
                ext: '.min.css'
            }
        },
        htmlmin: {                                     // Task
            dist: {                                      // Target
                options: {                                 // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '',
                        src: ['app/index.html'],
                        dest: ''
                    },
                    {
                        expand: true,
                        cwd: '',
                        src: ['app/views/*.html'],
                        dest: ''
                    },
                    {
                        expand: true,
                        cwd: '',
                        src: ['app/views/directives/*.html'],
                        dest: ''
                    },
                    {
                        expand: true,
                        cwd: '',
                        src: ['app/views/*/*.html'],
                        dest: ''
                    }
                ]
            }
        }
    });


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    // Default task(s).
    //grunt.registerTask('default', ['uglify']);

};
